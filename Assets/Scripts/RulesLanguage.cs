﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RulesLanguage : MonoBehaviour
{
  // Start is called before the first frame update
  void Start()
  {
    TextAsset reader;

    switch (PlayerPrefs.GetInt("lang"))
    {
      case 0:
        reader = Resources.Load<TextAsset>("Txt/rules_I500_pt");
        break;
      case 1:
        reader = Resources.Load<TextAsset>("Txt/rules_I500_en");
        break;
      case 2:
        reader = Resources.Load<TextAsset>("Txt/rules_I500_fr");
        break;
      case 3:
        reader = Resources.Load<TextAsset>("Txt/rules_I500_es");
        break;
      default:
        reader = Resources.Load<TextAsset>("Txt/rules_I500_pt");
        break;
    }

    gameObject.GetComponent<TextMeshProUGUI>().text = reader.ToString();
  }

  // Update is called once per frame
  void Update()
  {

  }
}
