﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleBackground : MonoBehaviour
{
    public Toggle theToggle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeBackground() {
        if (theToggle.isOn) {
            gameObject.SetActive(true);
            Debug.Log("true");
        }else {
            gameObject.SetActive(false);
            Debug.Log("false");
        }
    }
}
