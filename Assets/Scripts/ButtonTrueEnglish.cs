﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonTrueEnglish : MonoBehaviour
{
    public Texture2D trueImg;
  // Start is called before the first frame update
  void Start()
  {
    if (PlayerPrefs.GetInt("lang") == 1) {
        gameObject.GetComponent<Image>().sprite = Sprite.Create(trueImg, new Rect(0.0f, 0.0f, trueImg.width, trueImg.height), new Vector2(0.5f, 0.5f), 100.0f);
    }
  }

  // Update is called once per frame
  void Update()
  {

  }
}
