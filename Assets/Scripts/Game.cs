﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Game : MonoBehaviour
{
  public GameObject cartaObj, textoCartaObj, imagemCartaObj, buttonsTF, buttonMove, botaoResposta, botaoProxCarta, instructionsObj;
  public GameObject buttonSelectTrueObj, buttonSelectFalseObj, CheckMarkTrueObj, CheckMarkFalseObj;

  public GameObject matchInfoObj, canvasDirecao;
  public Texture2D molduraCarta, buttonTrueSelected, buttonTrueSelectedEnglish, buttonTrueNotSelectedEnglish, buttonFalseSelected, buttonTrueNotSelected, buttonFalseNotSelected, highlightAcertou, highlightErrou, imgTrue, imgFalse, imgCerto, imgErrado;

  // Start is called before the first frame update
  void Start()
  {
    matchInfoObj.GetComponent<TextMeshProUGUI>().text = "Código da partida: " + PlayerPrefs.GetInt("seed") + " / Carta de número: " + PlayerPrefs.GetInt("cartaNumero");
  }

  // Update is called once per frame
  void Update()
  {

  }

  public void IniciarJogo()
  {
    cartaObj.GetComponent<Image>().sprite = Sprite.Create(molduraCarta, new Rect(0.0f, 0.0f, molduraCarta.width, molduraCarta.height), new Vector2(0.5f, 0.5f), 100.0f);
    cartaObj.GetComponent<Button>().interactable = false;
    switch (PlayerPrefs.GetInt("lang"))
    {
      case 0:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Todos respondem";
        break;
      case 1:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "All players respond";
        break;
      case 2:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Tous les joueurs répondent";
        break;
      case 3:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Todos responden";
        break;
      default:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Todos checam a resposta";
        break;
    }

    textoCartaObj.SetActive(true);
    imagemCartaObj.SetActive(true);
    buttonsTF.SetActive(true);
  }

  public void SelectTrue()
  {
    PlayerPrefs.SetString("botaoResposta", "true");
    if (PlayerPrefs.GetInt("lang") == 1)
    {
      buttonSelectTrueObj.GetComponent<Image>().sprite = Sprite.Create(buttonTrueSelectedEnglish, new Rect(0.0f, 0.0f, buttonTrueSelectedEnglish.width, buttonTrueSelectedEnglish.height), new Vector2(0.5f, 0.5f), 100.0f);
    }
    else
    {
      buttonSelectTrueObj.GetComponent<Image>().sprite = Sprite.Create(buttonTrueSelected, new Rect(0.0f, 0.0f, buttonTrueSelected.width, buttonTrueSelected.height), new Vector2(0.5f, 0.5f), 100.0f);
    }
    buttonSelectFalseObj.GetComponent<Image>().sprite = Sprite.Create(buttonFalseNotSelected, new Rect(0.0f, 0.0f, buttonFalseNotSelected.width, buttonFalseNotSelected.height), new Vector2(0.5f, 0.5f), 100.0f);
    botaoResposta.SetActive(true);

    switch (PlayerPrefs.GetInt("lang"))
    {
      case 0:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Todos checam a resposta";
        break;
      case 1:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Everyone checks the answer";
        break;
      case 2:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Tout le monde vérifie la réponse";
        break;
      case 3:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Todos verifican la respuesta";
        break;
      default:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Todos checam a resposta";
        break;
    }
  }
  public void SelectFalse()
  {
    PlayerPrefs.SetString("botaoResposta", "false");
    if (PlayerPrefs.GetInt("lang") == 1)
    {
      buttonSelectTrueObj.GetComponent<Image>().sprite = Sprite.Create(buttonTrueNotSelectedEnglish, new Rect(0.0f, 0.0f, buttonTrueNotSelectedEnglish.width, buttonTrueNotSelectedEnglish.height), new Vector2(0.5f, 0.5f), 100.0f);
    }
    else
    {
      buttonSelectTrueObj.GetComponent<Image>().sprite = Sprite.Create(buttonTrueNotSelected, new Rect(0.0f, 0.0f, buttonTrueNotSelected.width, buttonTrueNotSelected.height), new Vector2(0.5f, 0.5f), 100.0f);
    }
    buttonSelectFalseObj.GetComponent<Image>().sprite = Sprite.Create(buttonFalseSelected, new Rect(0.0f, 0.0f, buttonFalseSelected.width, buttonFalseSelected.height), new Vector2(0.5f, 0.5f), 100.0f);
    botaoResposta.SetActive(true);

    switch (PlayerPrefs.GetInt("lang"))
    {
      case 0:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Todos checam a resposta";
        break;
      case 1:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Everyone checks the answer";
        break;
      case 2:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Tout le monde vérifie la réponse";
        break;
      case 3:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Todos verifican la respuesta";
        break;
      default:
        instructionsObj.GetComponent<TextMeshProUGUI>().text = "Todos checam a resposta";
        break;
    }
  }

  public void verResposta()
  {
    if (CardGenerate.cartaAtual.GetResposta() == true && PlayerPrefs.GetString("botaoResposta") == "true")
    {
      CheckMarkTrueObj.GetComponent<Image>().sprite = Sprite.Create(highlightAcertou, new Rect(0.0f, 0.0f, highlightAcertou.width, highlightAcertou.height), new Vector2(0.5f, 0.5f), 100.0f);
      switch (PlayerPrefs.GetInt("lang"))
      {
        case 0:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#00ff00> Acertou!</color>";
          break;
        case 1:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#00ff00> Right!</color>";
          break;
        case 2:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#00ff00> Correct!</color>";
          break;
        case 3:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#00ff00> Correcto!</color>";
          break;
        default:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#00ff00> Acertou!</color>";
          break;
      }
      buttonMove.SetActive(true);
      CheckMarkTrueObj.SetActive(true);
      //canvasDirecao.SetActive(true); 
    }
    if (CardGenerate.cartaAtual.GetResposta() == false && PlayerPrefs.GetString("botaoResposta") == "false")
    {
      CheckMarkFalseObj.GetComponent<Image>().sprite = Sprite.Create(highlightAcertou, new Rect(0.0f, 0.0f, highlightAcertou.width, highlightAcertou.height), new Vector2(0.5f, 0.5f), 100.0f);
      switch (PlayerPrefs.GetInt("lang"))
      {
        case 0:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#00ff00> Acertou!</color>";
          break;
        case 1:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#00ff00> Right!</color>";
          break;
        case 2:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#00ff00> Correct!</color>";
          break;
        case 3:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#00ff00> Correcto!</color>";
          break;
        default:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#00ff00> Acertou!</color>";
          break;
      }
      buttonMove.SetActive(true);
      CheckMarkFalseObj.SetActive(true);
      //canvasDirecao.SetActive(true);
    }

    if (CardGenerate.cartaAtual.GetResposta() == true && PlayerPrefs.GetString("botaoResposta") == "false")
    {
      CheckMarkFalseObj.GetComponent<Image>().sprite = Sprite.Create(highlightErrou, new Rect(0.0f, 0.0f, highlightErrou.width, highlightErrou.height), new Vector2(0.5f, 0.5f), 100.0f);
      switch (PlayerPrefs.GetInt("lang"))
      {
        case 0:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#ff0000> Errou!</color>";
          break;
        case 1:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#ff0000> Wrong!</color>";
          break;
        case 2:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#ff0000> Incorrect!</color>";
          break;
        case 3:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#ff0000> Incorrecto!</color>";
          break;
        default:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#ff0000> Errou!</color>";
          break;
      }
      CheckMarkFalseObj.SetActive(true);
    }
    if (CardGenerate.cartaAtual.GetResposta() == false && PlayerPrefs.GetString("botaoResposta") == "true")
    {
      CheckMarkTrueObj.GetComponent<Image>().sprite = Sprite.Create(highlightErrou, new Rect(0.0f, 0.0f, highlightErrou.width, highlightErrou.height), new Vector2(0.5f, 0.5f), 100.0f);
      switch (PlayerPrefs.GetInt("lang"))
      {
        case 0:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#ff0000> Errou!</color>";
          break;
        case 1:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#ff0000> Wrong!</color>";
          break;
        case 2:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#ff0000> Incorrect!</color>";
          break;
        case 3:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#ff0000> Incorrecto!</color>";
          break;
        default:
          instructionsObj.GetComponent<TextMeshProUGUI>().text = "<color=#ff0000> Errou!</color>";
          break;
      }
      CheckMarkTrueObj.SetActive(true);
    }

    buttonsTF.SetActive(true);
    buttonSelectTrueObj.GetComponent<Button>().interactable = false;
    buttonSelectFalseObj.GetComponent<Button>().interactable = false;
    botaoResposta.SetActive(false);
    botaoProxCarta.SetActive(true);

  }

  public void proximaCarta()
  {
    PlayerPrefs.DeleteKey("botaoResposta");
    //CardGenerate.cartaAtual.GetResposta();
    PlayerPrefs.SetInt("cartaNumero", ((PlayerPrefs.GetInt("cartaNumero")) + 1));
    SceneManager.LoadScene("TelaCarta");
  }

  public void botaoSairDaPartida()
  {
    PlayerPrefs.DeleteKey("botaoResposta");
    PlayerPrefs.DeleteKey("seed");
    PlayerPrefs.DeleteKey("cartaNumero");
    SceneManager.LoadScene("TelaMenu");
  }


}
