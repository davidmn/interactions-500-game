﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class ButtonTextLanguage : MonoBehaviour
{
    public string ButtonPt, ButtonEn, ButtonFr, ButtonEs;
    // Start is called before the first frame update
    void Start()
    {
        switch (PlayerPrefs.GetInt("lang"))
        {
            case 0:
            gameObject.GetComponent<TextMeshProUGUI>().text = ButtonPt;  
            break;
            case 1:
            gameObject.GetComponent<TextMeshProUGUI>().text = ButtonEn;
            break;
            case 2:
            gameObject.GetComponent<TextMeshProUGUI>().text = ButtonFr;
            break;
            case 3:
            gameObject.GetComponent<TextMeshProUGUI>().text = ButtonEs;
            break;
    

            default:
            break;
        }    
    }
}
