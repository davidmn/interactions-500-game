﻿using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class ButtonShare : MonoBehaviour
{
  public void Share()
  {
      StartCoroutine(TakeSSAndShare());
  }

  private IEnumerator TakeSSAndShare()
  {
    yield return new WaitForEndOfFrame();

    Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
    ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
    ss.Apply();

    string filePath = Path.Combine(Application.temporaryCachePath, "screenshot.png");
    File.WriteAllBytes(filePath, ss.EncodeToPNG());

    // To avoid memory leaks
    Destroy(ss);
#if UNITY_IOS
    new NativeShare().AddFile(filePath).SetSubject("Download Interactions500").SetText("Download Interactions500! App Store link: https://apps.apple.com/us/app/interactions-500/id1509337864").Share();
    UnityEngine.Debug.Log("shared for IOS");
#endif
#if UNITY_ANDROID
    new NativeShare().AddFile(filePath).SetSubject("Download Interactions500").SetText("Download Interactions500! Google Play link: https://play.google.com/store/apps/details?id=com.LDSE.i500").Share();
    UnityEngine.Debug.Log("shared for Android");
#endif
  }
}

