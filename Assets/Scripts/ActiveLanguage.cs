﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActiveLanguage : MonoBehaviour
{
    public int langCode;
    public Texture2D capacete;
    // Start is called before the first frame update
    void Start()
    {
    if (PlayerPrefs.GetInt("lang") == langCode) {
        gameObject.GetComponent<Image>().sprite = Sprite.Create(capacete, new Rect(0.0f, 0.0f, capacete.width, capacete.height), new Vector2(0.5f, 0.5f), 100.0f);
    }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
