﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Direcao : MonoBehaviour
{
  // Start is called before the first frame update
  public Texture2D oeste, noroeste, norte, nordeste, leste;

  void Start()
  {

    Random.InitState(System.Environment.TickCount);
    int valor = Random.Range(1, 6);

    switch (valor)
    {
      case 1:
        gameObject.GetComponent<Image>().sprite = Sprite.Create(oeste, new Rect(0.0f, 0.0f, oeste.width, oeste.height), new Vector2(0.5f, 0.5f), 100.0f);
        break;
      case 2:
        gameObject.GetComponent<Image>().sprite = Sprite.Create(noroeste, new Rect(0.0f, 0.0f, noroeste.width, noroeste.height), new Vector2(0.5f, 0.5f), 100.0f);
        break;
      case 3:
        gameObject.GetComponent<Image>().sprite = Sprite.Create(norte, new Rect(0.0f, 0.0f, norte.width, norte.height), new Vector2(0.5f, 0.5f), 100.0f);
        break;
      case 4:
        gameObject.GetComponent<Image>().sprite = Sprite.Create(nordeste, new Rect(0.0f, 0.0f, nordeste.width, nordeste.height), new Vector2(0.5f, 0.5f), 100.0f);
        break;
      case 5:
        gameObject.GetComponent<Image>().sprite = Sprite.Create(leste, new Rect(0.0f, 0.0f, leste.width, leste.height), new Vector2(0.5f, 0.5f), 100.0f);
        break;

      default:
        break;
    }

  }

  // Update is called once per frame
  void Update()
  {

  }
}
