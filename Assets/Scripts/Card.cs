﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Card
{
    private String id;
    private String pergunta;
    private bool resposta;
    private Sprite imagem;

    private int numDeck, numCarta;
    public int NumDeck { get => numDeck; set => numDeck = value; }
    public int NumCarta { get => numCarta; set => numCarta = value; }

    public Card(String id)
    {
        this.id = id;

        char[] delimiterChars = { 'A', '-' };
        string[] valoresId = this.id.Split(delimiterChars);

        // NumDeck = int.Parse(valoresId[1]);
        NumDeck = 001;
        NumCarta = int.Parse(valoresId[2]);
        

        SetPergunta();
        SetResposta();
        SetImagem();
    }

    public String GetId() {
        return id;
    }

    public void SetPergunta()
    {
        TextAsset reader;

        switch (PlayerPrefs.GetInt("lang"))
        {
        case 0:
            reader = Resources.Load<TextAsset>("Txt/deck_pt");
            break;
        case 1:
            reader = Resources.Load<TextAsset>("Txt/deck_en");
            break;
        case 2:
            reader = Resources.Load<TextAsset>("Txt/deck_fr");
            break;
        case 3:
            reader = Resources.Load<TextAsset>("Txt/deck_es");
            break;
        default:
            reader = Resources.Load<TextAsset>("Txt/deck_pt");
            break;
        }

        string TextoDoDeck = reader.ToString();
        string toFind1 = "A0" + NumDeck.ToString("00") + "-" + NumCarta.ToString("000") + ":";
        int start = TextoDoDeck.IndexOf(toFind1) + toFind1.Length;
        int end = TextoDoDeck.IndexOf("A0" + NumDeck.ToString("00"), start);
        string StringPronta = TextoDoDeck.Substring(start, end - start);
        pergunta = StringPronta;
    }

    public String GetPergunta(){
        return pergunta;
    }

    public void SetResposta() {
        // String str = id.Substring(id.Length - 1);
        // int val = int.Parse(str);

        // if (val % 2 == 0) {
        //     resposta = false;
        // } else {
        //     resposta = true;
        // }
        if (NumCarta % 2 == 0) {
            resposta = false;
        } else {
            resposta = true;
        }
    }

    public bool GetResposta() {
        return resposta;
    }

    public void SetImagem() {
        if (Resources.Load<Texture2D>("Img/Cards/" + id) != null){
            Texture2D tex = Resources.Load<Texture2D>("Img/Cards/" + id);
            imagem = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        } else {
            Texture2D tex = new Texture2D(1, 1);
            tex.SetPixel(1, 1, Color.clear);
            tex.Apply();
            imagem = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        }
    }

    public Sprite GetImagem() {
        return imagem;
    }
}
